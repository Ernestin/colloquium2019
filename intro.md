# Intro

> Auch wenn Separation of Concerns (SoC) und Single Responsibility Principle (SRP) nicht exakt das gleiche Bedeuten, so ist Ihre Schnittmenge größer
> als das was sie Trennt.
> Für den Unterschied siehe ...

## Separation of Concerns (SoC)
Unter Separation of Concerns (SoC) versteht man das Aufteilen eines Computerprogramms in bestimmte Merkmale, deren Funktionalität sich so wenig wie möglich überschneiden soll. Ein **Anliegen** oder besser eine **Verantwortlichkeit** ist jeder Schwerpunkt in einem Programm, der eine gewisse autonome Verantwortung über den **Programmcode** und dessen Querbezüge übernimmt. Also klar abgrenzbare Bereiche im **Programmcode** denen man bestimmte, wohl definierte **Verantwortlichkeiten** über die in ihnen realisierten Anliegen zuschreibt.
In der Regel ist ein **Concern** gleichbedeutend mit **Funktion** oder **Verhaltensweisen**. Programmiersprachen bieten für SoC, Funktionen, Module, Klassen, Mixin und vorallem auch Namensräume.
http://en.wikipedia.org/wiki/Separation_of_concerns

---
## Single Responsibility Principle (SRP)
Prinzip der einmaligen **Verantwortung** (Single Responsibility Principle, SRP) - Jedes Objekt sollte eine einmalige Verantwortung haben und alle seine Dienste sollten eng mit dieser Verantwortung verbunden sein. In gewisser Hinsicht wird Kohäsion als Synonym für SRP angesehen.
http://en.wikipedia.org/wiki/Single_responsibility_principle


https://weblogs.asp.net/arturtrosin/separation-of-concern-vs-single-responsibility-principle-soc-vs-srp

https://www.bing.com/images/search?view=detailV2&ccid=OmoyE%2FHe&id=BB3DB3FB011FA6CA86F502FC3EC54396ECE86BDA&thid=OIP.OmoyE_HeWWpc94JQ06jEfQHaFj&mediaurl=https%3A%2F%2Fimage.slidesharecdn.com%2Fmicroservices-151002053503-lva1-app6891%2F95%2Fmicroservices-die-architektur-fr-agileentwicklung-26-638.jpg%3Fcb%3D1443764167&exph=479&expw=638&q=softwarearchitektur+skalierung&simid=608055355829651114&selectedindex=2&adlt=strict&vt=0&sim=11
https://www.bing.com/images/search?view=detailV2&ccid=UZrdbI2l&id=106CFCC67CD4F6F9CB4BDBD79B6974602EF70C0F&thid=OIP.UZrdbI2lAta1wNDuEtQDGgHaEK&mediaurl=https%3a%2f%2fimage.slidesharecdn.com%2fdevops-alexander-pacnik-brownbag-29-09-2016-160930113932%2f95%2fdevops-17-638.jpg%3fcb%3d1475235804&exph=359&expw=638&q=softwarearchitektur+skalierung&simid=607992155429603589&selectedIndex=5&adlt=strict
