@BOOK{Riel1996,
	AUTHOR = {Riel, Arthur J.},
	YEAR = {1996},
	TITLE = {Object-oriented Design Heuristics - },
	EDITION = {},
	ISBN = {978-0-201-63385-6},
	PUBLISHER = {Addison-Wesley Publishing Company},
	ADDRESS = {Reading},
}

Riel, Arthur J.: Object-oriented Design Heuristics. Reading: Addison-Wesley Publishing Company, 1996.
https://www.amazon.de/dp/020163385X

---

@BOOK{Martin2011,
	AUTHOR = {Martin, Robert C.},
	YEAR = {2011},
	TITLE = {Agile Software Development - Principles, Patterns, and Practices},
	EDITION = {01. Aufl.},
	ISBN = {978-0-132-76058-4},
	PUBLISHER = {Pearson Prentice Hall},
	ADDRESS = {New Jersey},
}

Martin, Robert C.: Agile Software Development : Principles, Patterns, and Practices. 01. Aufl.. New Jersey: Pearson Prentice Hall, 2011.
https://www.amazon.de/dp/0132760584

