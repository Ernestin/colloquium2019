# Cross-Cutting Concern (CCC)

Die kostengünstige und termingerechte Entwicklung und Wartung qualitativ hochwertiger Software ist das Primärziel der Softwaretechnik. 


Software einfach, qualitativ hochwertig ohne große Komplexität und möglichst mit Einsatz von wenigen Technologien, Frameworks zu erstellen ist üblicherweise der Wunsch; 
und das Ziel der Softwaretechnik.

Um dieses Ziel zu erreichen, ist eine möglichst gut modularisierte Software mit einer möglichst geringen Komplexität notwendig. In einem konventionellen System, wobei hier auch die objektorientierten Ansätze hinzugehören, können Kernfunktionalitäten für sich allein betrachtet nach den Regeln der Kunst sauber in Module getrennt werden. Es gibt jedoch Anforderungen wie Fehlerbehandlung, Performance und Sicherheit in jedem System, die alle Kernfunktionalitäten betreffen und sich deshalb nicht eindeutig einem Software-Modul zuordnen lassen. Dies führt dazu, dass Fragmente solcher querschnittlicher Funktionen aufgrund fehlender Kohäsion nicht zugeordnet und ungekapselt im ganzen Code verstreut sind.

Meist sind es nichtfunktionale Anforderungen an Software wie etwa Sicherheitsaspekte, die bei konventioneller Programmierung quer verstreut über den gesamten Code realisiert werden müssen.

Diese Anforderungen verhindern in Software-Systemen eine ideale Modularisierung und sorgen für aufwändigere Verständlichkeit, Wiederverwendbarkeit und Pflege. Verantwortlich hierfür ist bei konventionellen Programmiersprachen die Systemdekomposition, die in modularen Systemen nur eine, in objektorientierten noch eine zweite Dimension zulässt. In diesem Zusammenhang spricht man auch von dominanter Dekomposition, d. h. ein natürlicherweise mehrdimensionales Problem muss in ein Modell mit weniger Dimensionen gepresst werden. 
